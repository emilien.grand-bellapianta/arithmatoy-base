# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:    
    return n * "S" + "0"
    pass


def S(n: str) -> str:
    return "S" + n
    pass


def addition(a: str | int,  b: str | int) -> str:
    if (type(a) == int):
        a = nombre_entier(a)
    if (type(b) == int):
        b = nombre_entier(b)
    return "S" * (len(a) + len(b) - 2) + "0"
    pass


def multiplication(a: str | int, b: str | int) -> str:
    if (type(a) == int):
        a = nombre_entier(a)
    if (type(b) == int):
        b = nombre_entier(b)
    return "S"*((len(a) - 1) * (len(b) - 1)) + "0"
    pass


def facto_ite(n: int) -> int:
    x = 1
    for i in range(1, n + 1):
        x *= i
    return x 
    pass


def facto_rec(n: int) -> int:
    if (n == 0):
        return 1;
    else:
        return n * facto_rec(n - 1);
    pass


def fibo_rec(n: int) -> int:
    pass


def fibo_ite(n: int) -> int:
    pass


def golden_phi(n: int) -> int:
    pass


def sqrt5(n: int) -> int:
    pass


def pow(a: float, n: int) -> float:
    pass
